import {mapObj, compose} from './util.js';

(function () {
  // date utility functions
  const now = () => new Date();
  const getHoursMinutesAndSeconds = (date) => ({
    hours: date.getHours(),
    minutes: date.getMinutes(),
    seconds: date.getSeconds()
  });


  const prependZeroIfNeeded = (value) => value < 10 ? `0${value}` : value;
  const prependZeroOnClock = mapObj(prependZeroIfNeeded);

  const addTimezone = (time) => {
    const timezone = time.hours <= 12 ? 'AM' : 'PM';
    return ({
      ...time,
      timezone
    });
  }
  const getClock = ({hours, minutes, seconds, timezone}) => `${hours}:${minutes}:${seconds} ${timezone}`;
  const print = (formattedValue) => document.getElementById('declarative-clock').innerText = formattedValue;

  const printClock = compose(
    print,
    getClock,
    addTimezone,
    prependZeroOnClock,
    getHoursMinutesAndSeconds,
    now
  );

  setInterval(printClock, 1000);

})();
