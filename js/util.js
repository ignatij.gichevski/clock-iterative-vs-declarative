// utility functions
const mapObj = (fn) => (arg) => {
  const newObj = {};
  for (const key in arg) {
    if (arg.hasOwnProperty(key)) {
      newObj[key] = fn(arg[key]);
    }
  }
  return newObj;
}
const compose = (...fns) => (arg) =>
  fns.reduceRight((val, fn) => fn(val), arg)

export {mapObj, compose};
