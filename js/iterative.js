(function () {
  const printClock = function () {
    let date = new Date();
    let hours = date.getHours();
    let minutes = date.getMinutes();
    let seconds = date.getSeconds();

    let hoursText = '';
    let minutesText = '';
    let secondsText = '';
    let timezone = '';

    // get text representation of hours
    if (hours < 10) {
      hoursText = `0${hours}`;
    } else {
      hoursText = String(hours);
    }

    // get text representation of timezone
    if (hours < 12) {
      timezone = 'AM';
    }

    // get text representation of minutes
    if (minutes < 10) {
      minutesText = `0${minutes}`;
    } else {
      minutesText = String(minutes);
    }

    // get text representation of seconds
    if (seconds < 10) {
      secondsText = `0${seconds}`;
    } else {
      secondsText = String(seconds);
    }

    document.getElementById(`imperative-clock`).innerText = `${hoursText}:${minutesText}:${secondsText} ${timezone}`;
  }

  setInterval(printClock, 1000);
})()


